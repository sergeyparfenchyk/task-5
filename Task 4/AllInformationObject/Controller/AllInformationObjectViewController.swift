//
//  AllInformationObjectViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 7.03.22.
//

import UIKit
import MapKit

// MARK: Constants

private extension ConstantString {
    static let titleRouteButton = "Построить маршрут"
}

private extension ConstantColor {
    static let routeButton = UIColor.systemGreen
}

private struct ConstantSize {
    static let heightRouteButton = 70.0
    static let spacingStackView = 20.0
    static let trailingAndLeadingOffsetStackView = 10.0
    static let topOffsetStackView = 20.0
}

class AllInformationObjectViewController: UIViewController {

    // MARK: Public Properties

    var typeObject = TypeObject.infobox
    var object: Any?
    var locationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)

    // MARK: Private Properties

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = ConstantSize.spacingStackView
        stackView.axis = .vertical
        return stackView
    }()

    private lazy var routeButton: UIButton = {
        let routeButton = UIButton()
        routeButton.backgroundColor = ConstantColor.routeButton
        routeButton.setTitle(ConstantString.titleRouteButton, for: .normal)
        routeButton.addTarget(self, action: #selector(openMapForRoute), for: .touchUpInside)
        return routeButton
    }()

    private var titleObjects = [""]
    private var descriptionObjects = [""]

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.background
        addSubviews()
        setConstraints()
        createDetailList()
    }

    // MARK: Private Metchod

    private func addSubviews() {
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        view.addSubview(routeButton)
    }

    private func setConstraints() {
        scrollView.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalTo(routeButton.snp.top)
        }
        stackView.snp.makeConstraints { make in
            make.trailing.leading.equalTo(view).offset(ConstantSize.trailingAndLeadingOffsetStackView)
            make.top.equalToSuperview().offset(ConstantSize.topOffsetStackView)
            make.bottom.equalToSuperview()
        }
        routeButton.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(ConstantSize.heightRouteButton)
            make.bottom.equalToSuperview()
        }
    }

    private func createDetailList() {
        switch typeObject {
        case .atm:
            if let object = object as? ATMResponse.DataClass.ATM {
                titleObjects = ATMResponse.DataClass.ATM.detailTitle
                descriptionObjects = object.getDetailDescription()
                addDetailOnList()
            }
        case .infobox:
            if let object = object as? InfoBoxResponse.DataClass.Infobox {
                titleObjects = InfoBoxResponse.DataClass.Infobox.detailTitle
                descriptionObjects = object.getDetailDescription()
                addDetailOnList()
            }
        case .branch:
            if let object = object as? BranchResponse.DataClass.Branch {
                titleObjects = BranchResponse.DataClass.Branch.detailTitle
                descriptionObjects = object.getDetailDescription()
                addDetailOnList()
            }
        }
    }

    private func addDetailOnList() {
        for (index, titleObject) in titleObjects.enumerated() {
            let cellView = CellView()
            stackView.addArrangedSubview(cellView)
            cellView.titleLable.text = titleObject
            cellView.descriptionLable.text = descriptionObjects[index]
        }
    }

    @objc private func openMapForRoute() {
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        let placemark = MKPlacemark(coordinate: locationCoordinate2D)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.openInMaps(launchOptions: launchOptions)
    }
}
