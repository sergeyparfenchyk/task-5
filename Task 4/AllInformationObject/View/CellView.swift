//
//  CellView.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 7.03.22.
//

import UIKit

// MARK: Constants

private struct ConstantSize {
    static let numberOfLinesTitle = 3
    static let numberOfLinesDescription = 10
    static let sizeFontDescription = 15.0
}

class CellView: UIView {

    // MARK: Private Properties
    private(set) lazy var titleLable: UILabel = {
        let titleLable = UILabel()
        titleLable.numberOfLines = ConstantSize.numberOfLinesTitle
        return titleLable
    }()

    private(set) lazy var descriptionLable: UILabel = {
        let descriptionLable = UILabel()
        descriptionLable.textColor = .systemGray2
        descriptionLable.font = UIFont.boldSystemFont(ofSize: ConstantSize.sizeFontDescription)
        descriptionLable.numberOfLines = ConstantSize.numberOfLinesDescription
        return descriptionLable
    }()

    // MARK: Initialize
    init() {
        super.init(frame: .zero)
        addSubviews()
        setConstraints()
    }

    // MARK: Private Method
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addSubviews() {
        addSubview(titleLable)
        addSubview(descriptionLable)
    }

    private func setConstraints() {
        titleLable.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview()
        }
        descriptionLable.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(titleLable.snp.bottom)
            make.bottom.equalToSuperview()
        }
    }
}
