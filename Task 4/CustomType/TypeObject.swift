//
//  TypeObject.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 7.03.22.
//

import Foundation

enum TypeObject: String {
    case atm = "Банкоматы"
    case infobox = "Инфокиоски"
    case branch = "Подразделения банка"
}
