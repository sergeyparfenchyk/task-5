//
//  ContainerViewControllersDelegate.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 5.03.22.
//

import Foundation

protocol ContainerViewControllersDelegate: AnyObject {
    func updateData(_ containerViewControllers: ContainerViewControllers)
    func changeVisibleObjects(_ containerViewControllers: ContainerViewControllers)
}
