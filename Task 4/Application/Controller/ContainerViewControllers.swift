//
//  ContainerViewControllers.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 22.02.22.
//

import UIKit
import SnapKit
import Network

// MARK: Constants

private extension ConstantString {
    static let atmS = "Беларусбанк"
    static let map = "Карта"
    static let list = "Список"
    static let errorInternetTitle = "Интернет-соединение отсутствует"
    static let errorInternetDescription = "Приложение не работает без доступа к интернету."
    static let errorInternetButtonAllert = "Хорошо"
}

private struct ConstantSize {
    static let heighMapOrListSegmentedControl = 30.0
}

class ContainerViewControllers: UIViewController {

    // MARK: Public Properties
    weak var delegate: ContainerViewControllersDelegate?

    // MARK: Private Properties
    private(set) var hideObjects: Set<TypeObject> = []
    private let atmResponse = ATMResponse()
    private let infoBoxResponse = InfoBoxResponse()
    private let branchResponse = BranchResponse()
    private var problems: Set<TypeObject> = []

    private lazy var updateRightButton = UIBarButtonItem(barButtonSystemItem: .refresh,
                                                         target: self,
                                                         action: #selector(updateData))

    private lazy var filterDataBarButtonItem = UIBarButtonItem(systemItem: .edit)

    private lazy var mapViewController: MapViewController = {
        let mapViewController = MapViewController()
        mapViewController.atmResponse = self.atmResponse
        mapViewController.branchResponse = self.branchResponse
        mapViewController.infoBoxResponse = self.infoBoxResponse
        mapViewController.hideObjects = self.hideObjects
        return mapViewController
    }()

    private lazy var listViewController = ListViewController()

    private lazy var mapAction: UIAction = {
        let mapAction = UIAction(title: ConstantString.map) {(_) in
            self.remove(child: self.listViewController)
            self.mapViewController.atmResponse = self.atmResponse
            self.mapViewController.branchResponse = self.branchResponse
            self.mapViewController.infoBoxResponse = self.infoBoxResponse
            self.mapViewController.hideObjects = self.hideObjects
            self.add(child: self.mapViewController)
        }
        return mapAction
    }()

    private lazy var listAction: UIAction = {
        let listAction = UIAction(title: ConstantString.list) {(_) in
            self.remove(child: self.mapViewController)
            self.listViewController.atmResponse = self.atmResponse
            self.listViewController.branchResponse = self.branchResponse
            self.listViewController.infoBoxResponse = self.infoBoxResponse
            self.listViewController.hideObjects = self.hideObjects
            self.add(child: self.listViewController)
        }
        return listAction
    }()

    private lazy var mapOrListSegmentedControl: UISegmentedControl = {
        let mapOrListSegmentedControl = UISegmentedControl(frame: .zero,
                                                           actions: [mapAction, listAction])
        mapOrListSegmentedControl.selectedSegmentIndex = 0
        view.addSubview(mapOrListSegmentedControl)
        mapOrListSegmentedControl.snp.makeConstraints { make in
            make.height.equalTo(ConstantSize.heighMapOrListSegmentedControl)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
        }
        return mapOrListSegmentedControl
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .large)
        view.addSubview(activityIndicatorView)
        activityIndicatorView.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
        return activityIndicatorView
    }()

    private lazy var problemAlertController: UIAlertController = {
        let problemAlertController = UIAlertController(title: ConstantString.errorDownload,
                                                       message: "",
                                                       preferredStyle: .alert)
        problemAlertController.addAction(UIAlertAction(title: ConstantString.good, style: .default))
        return problemAlertController
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.background
        title = ConstantString.atmS
        add(child: mapViewController)
        setNavigationController()
        fetchAllData()
    }

    // MARK: Private Methods

    private func setNavigationController() {
        navigationItem.rightBarButtonItem = updateRightButton
        navigationItem.leftBarButtonItem = filterDataBarButtonItem
        filterDataBarButtonItem.menu = createFilterMenu()
        updateRightButton.isEnabled = false
    }

    private func createFilterMenu() -> UIMenu {
        let infoBoxAction = UIAction(title: TypeObject.infobox.rawValue,
                                     state: hideObjects.contains(.infobox) ? .off : .on) { ( _ ) in
            if self.hideObjects.remove(.infobox) == nil {
                self.hideObjects.insert(.infobox)
            }
            self.delegate?.changeVisibleObjects(self)
            self.filterDataBarButtonItem.menu = self.createFilterMenu()
        }

        let atmAction = UIAction(title: TypeObject.atm.rawValue,
                                 state: hideObjects.contains(.atm) ? .off : .on) { ( _ ) in
            if self.hideObjects.remove(.atm) == nil {
                self.hideObjects.insert(.atm)
            }
            self.delegate?.changeVisibleObjects(self)
            self.filterDataBarButtonItem.menu = self.createFilterMenu()
        }

        let branchBoxAction = UIAction(title: TypeObject.branch.rawValue,
                                       state: hideObjects.contains(.branch) ? .off : .on) { ( _ ) in
            if self.hideObjects.remove(.branch) == nil {
                self.hideObjects.insert(.branch)
            }
            self.delegate?.changeVisibleObjects(self)
            self.filterDataBarButtonItem.menu = self.createFilterMenu()
        }

        let filterMenuItem = [atmAction, branchBoxAction, infoBoxAction]
        let filteMenu = UIMenu(title: "", image: nil, identifier: nil, options: [], children: filterMenuItem)

        return filteMenu
    }

    private func showActivityIndicatorView() {
        updateRightButton.isEnabled = false
        navigationController?.view.isUserInteractionEnabled = false
        view.isUserInteractionEnabled = false
        view.bringSubviewToFront(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }

    private func hideActivityIndicatorView() {
        navigationController?.view.isUserInteractionEnabled = true
        view.isUserInteractionEnabled = true
        activityIndicatorView.stopAnimating()
    }

    @objc private func updateData() {
        showActivityIndicatorView()
        DispatchQueue.global(qos: .userInitiated).async {[weak self] in
            self?.fetchATMResponse()
            DispatchQueue.main.sync { [weak self] in
                guard let self = self else { return }
                self.delegate?.updateData(self)
                self.hideActivityIndicatorView()
            }
        }
        let allDataGroup = DispatchGroup()
        DispatchQueue.global(qos: .background).async(group: allDataGroup) {[weak self] in
            self?.fetchBranchResponse()
        }
        DispatchQueue.global(qos: .background).async(group: allDataGroup) {[weak self] in
            self?.fetchInfoBoxResponse()
        }
        allDataGroup.notify(queue: DispatchQueue.main) {[weak self] in
            guard let self = self else { return }
            self.delegate?.updateData(self)
            self.updateRightButton.isEnabled = true
        }
    }

    private func fetchAllData() {
        showActivityIndicatorView()
        let allDataGroup = DispatchGroup()
        DispatchQueue.global().async(group: allDataGroup) {[weak self] in
            self?.fetchATMResponse()
        }
        DispatchQueue.global().async(group: allDataGroup) {[weak self] in
            self?.fetchBranchResponse()
        }
        DispatchQueue.global().async(group: allDataGroup) {[weak self] in
            self?.fetchInfoBoxResponse()
        }
        allDataGroup.notify(queue: DispatchQueue.main) {[weak self] in
            guard let self = self else { return }
            self.updateInterface()
            self.updateRightButton.isEnabled = true
        }
    }

    private func updateInterface() {
        delegate?.updateData(self)
        hideActivityIndicatorView()
        checkProblenDownload()
    }

    private func checkProblenDownload() {
        if problems.count > 0 {
            var messege = ""
            for problem in problems {
                messege += problem.rawValue + "\n"
            }
            problemAlertController.message = String(messege.dropLast())
            present(problemAlertController, animated: true)
        }
        problems.removeAll()
    }

    private func fetchATMResponse() {
        if let url = ATMResponse.getUrl() {
            do {
                let data = try Data(contentsOf: url)
                do {
                    let atm = try JSONDecoder().decode(ATMResponse.self, from: data)
                    atmResponse.data = atm.data
                } catch {
                    problems.insert(.atm)
                }
            } catch {
                problems.insert(.atm)
            }
        }
    }

    private func fetchBranchResponse() {
        if let url = BranchResponse.getUrl() {
            do {
                let data = try Data(contentsOf: url)
                do {
                    let branch = try JSONDecoder().decode(BranchResponse.self, from: data)
                    branchResponse.data = branch.data
                } catch {
                    problems.insert(.branch)
                }
            } catch {
                problems.insert(.branch)
            }
        }
    }

    private func fetchInfoBoxResponse() {
        if let url = InfoBoxResponse.getUrl() {
            do {
                let data = try Data(contentsOf: url)
                do {
                    let infoBox = try JSONDecoder().decode([InfoBoxResponse.DataClass.Infobox].self, from: data)
                    infoBoxResponse.data.infobox = infoBox
                } catch {
                    problems.insert(.infobox)
                }
            } catch {
                problems.insert(.infobox)
            }
        }
    }

    private func add(child viewController: UIViewController) {
        addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.snp.makeConstraints { make in
            make.top.equalTo(mapOrListSegmentedControl.snp.bottom)
            make.leading.equalTo(view.safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(view.safeAreaLayoutGuide.snp.trailing)
            make.bottom.equalTo(view.snp.bottom)
        }
        viewController.didMove(toParent: self)
    }

    private func remove(child viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
}
