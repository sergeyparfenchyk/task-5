//
//  MapViewControllerDataSource.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 6.03.22.
//

import Foundation

protocol MapViewControllerDataSource {
    func mapUpdateDate() -> [DataObjectForMap]
}
