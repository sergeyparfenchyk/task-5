//
//  MapViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 23.02.22.
//

import UIKit
import MapKit

// MARK: - Constants

private extension ConstantString {
    static let pointId = "PointId"
    static let locationTitleAlert = "Приложение не знает, где вы находитесь"
    static let locationDescriptionAlert = "Разрешить определять ваше местоположение: " +
                                          "это делается в настройках устройства."
    static let settingButttonAlert = "Настройки"
    static let cancelButttonAlert = "Отменить"
}

class MapViewController: UIViewController {

    // MARK: - Public Properties

    var atmResponse = ATMResponse()
    var infoBoxResponse = InfoBoxResponse()
    var branchResponse = BranchResponse()
    var hideObjects: Set<TypeObject> = []

    // MARK: - Private Properties

    private var atmObjects: [DataObjectForMap] = []
    private var infoBoxObjects: [DataObjectForMap] = []
    private var branchObjects: [DataObjectForMap] = []
    private let locationManager = CLLocationManager()
    private var objectsOnMap: Set<TypeObject> = []
    private lazy var mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.delegate = self
        return mapView
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mapView)
        mapView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
        locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            mapView.showsUserLocation = true
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if let containerViewControllers = parent as? ContainerViewControllers {
            containerViewControllers.delegate = self
        }
        super.viewWillAppear(animated)
        switch locationManager.authorizationStatus {
        case .notDetermined:
            requestOpenSetting()
        case .restricted:
            requestOpenSetting()
        case .denied:
            requestOpenSetting()
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            break
        @unknown default:
            break
        }
        setObjectsInMap()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: Private Methods

    private func requestOpenSetting() {
        let locationAlert = UIAlertController(title: ConstantString.locationTitleAlert,
                                              message: ConstantString.locationDescriptionAlert,
                                              preferredStyle: .alert)
        let settingButton = UIAlertAction(title: ConstantString.settingButttonAlert,
                                          style: .default,
                                          handler: { (_) -> Void in
            self.openSetting()
        })
        let cancelButton = UIAlertAction(title: ConstantString.cancelButttonAlert,
                                         style: .cancel,
                                         handler: nil)
        locationAlert.addAction(settingButton)
        locationAlert.addAction(cancelButton)
        present(locationAlert, animated: true, completion: nil)
    }

    private func openSetting() {
        if let appSettings = URL(string: UIApplication.openSettingsURLString),
           UIApplication.shared.canOpenURL(appSettings) {
            UIApplication.shared.open(appSettings)
        }
    }

    private func setObjectsInMap() {
        objectsOnMap.removeAll()
        removePointInMap(.atm)
        removePointInMap(.branch)
        removePointInMap(.infobox)
        if !hideObjects.contains(.atm) && !objectsOnMap.contains(.atm) {
            addPointInMap(atmObjects)
            objectsOnMap.insert(.atm)
        }
        if !hideObjects.contains(.branch) && !objectsOnMap.contains(.branch) {
            addPointInMap(branchObjects)
            objectsOnMap.insert(.branch)
        }
        if !hideObjects.contains(.infobox) && !objectsOnMap.contains(.infobox) {
            addPointInMap(infoBoxObjects)
            objectsOnMap.insert(.infobox)
        }
        if let location = mapView.userLocation.location {
            focusUser(location: location)
        }
    }

    private func updateObjectsInMap() {
        updateDate()
        if hideObjects.contains(.atm) && objectsOnMap.contains(.atm) {
            removePointInMap(.atm)
            objectsOnMap.remove(.atm)
        }
        if !hideObjects.contains(.atm) && !objectsOnMap.contains(.atm) {
            addPointInMap(atmObjects)
            objectsOnMap.insert(.atm)
        }

        if hideObjects.contains(.branch) && objectsOnMap.contains(.branch) {
            removePointInMap(.branch)
            objectsOnMap.remove(.branch)
        }
        if !hideObjects.contains(.branch) && !objectsOnMap.contains(.branch) {
            addPointInMap(branchObjects)
            objectsOnMap.insert(.branch)
        }

        if hideObjects.contains(.infobox) && objectsOnMap.contains(.infobox) {
            removePointInMap(.infobox)
            objectsOnMap.remove(.infobox)
        }
        if !hideObjects.contains(.infobox) && !objectsOnMap.contains(.infobox) {
            addPointInMap(infoBoxObjects)
            objectsOnMap.insert(.infobox)
        }
    }

    private func updateDate() {
        atmObjects = atmResponse.mapUpdateDate()
        infoBoxObjects = infoBoxResponse.mapUpdateDate()
        branchObjects = branchResponse.mapUpdateDate()
    }

    @objc private func hideCallout() {
        mapView.deselectAnnotation(nil, animated: true)
    }

    private func addPointInMap(_ objectForMap: [DataObjectForMap]) {
        for (index, object) in objectForMap.enumerated() {
            let pin = ObjectPointAnnotation()
            pin.idObject = index
            pin.typeObject = object.type
            pin.coordinate = object.locationCoordinate2D
            mapView.addAnnotation(pin)
        }
    }

    private func removePointInMap(_ typeObject: TypeObject) {
        let annotations = mapView.annotations
        for annotation in annotations {
            if let annotation = annotation as? ObjectPointAnnotation {
                if annotation.typeObject == typeObject {
                    mapView.removeAnnotation(annotation)
                }
            }
        }
    }

    private func focusUser(location: CLLocation) {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                            longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center,
                                        span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
        mapView.setRegion(region, animated: true)
    }

    @objc private func openAllInformationATM(button: DetailedButton) {
        let index = button.tag
        let allInformationObjectViewController = AllInformationObjectViewController()
        allInformationObjectViewController.modalPresentationStyle = .pageSheet
        switch button.typeObject {
        case .atm:
            allInformationObjectViewController.typeObject = .atm
            allInformationObjectViewController.object = atmObjects[index].object
            allInformationObjectViewController.locationCoordinate2D = atmObjects[index].locationCoordinate2D
            present(allInformationObjectViewController, animated: true)
        case .infobox:
            allInformationObjectViewController.typeObject = .infobox
            allInformationObjectViewController.object = infoBoxObjects[index].object
            allInformationObjectViewController.locationCoordinate2D = infoBoxObjects[index].locationCoordinate2D
            present(allInformationObjectViewController, animated: true)
        case .branch:
            allInformationObjectViewController.typeObject = .branch
            allInformationObjectViewController.object = branchObjects[index].object
            allInformationObjectViewController.locationCoordinate2D = branchObjects[index].locationCoordinate2D
            present(allInformationObjectViewController, animated: true)
        }
    }

    private func createCalloutView(_ dataObjectForMap: DataObjectForMap) -> UIView {
        let calloutView = InfoForСalloutView()
        calloutView.placeLable.text = dataObjectForMap.addressLine
        calloutView.workingHoursLable.text = dataObjectForMap.workTime
        calloutView.currencyLable.text = dataObjectForMap.currency
        calloutView.cashInLable.text = "\(ConstantString.serviceTypeCashIn): \(dataObjectForMap.cashIn)"
        calloutView.moreDetailedButton.typeObject = dataObjectForMap.type
        calloutView.moreDetailedButton.tag = dataObjectForMap.idObject
        calloutView.moreDetailedButton.addTarget(self, action: #selector(openAllInformationATM), for: .touchUpInside)
        calloutView.closeButton.addTarget(self, action: #selector(hideCallout), for: .touchUpInside)
        return calloutView
    }
}

// MARK: MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let objectPointAnnotation = annotation as? ObjectPointAnnotation else { return nil}
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: ConstantString.pointId)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = objectPointAnnotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: ConstantString.pointId)
        }
        let idObject = objectPointAnnotation.idObject
        let typeObject = objectPointAnnotation.typeObject
        view.canShowCallout = true
        var dataObjectForMap: DataObjectForMap
        switch typeObject {
        case .atm:
            view.markerTintColor = UIColor.green
            dataObjectForMap = atmObjects[idObject]
        case .infobox:
            view.markerTintColor = UIColor.magenta
            dataObjectForMap = infoBoxObjects[idObject]
        case .branch:
            view.markerTintColor = UIColor.yellow
            dataObjectForMap = branchObjects[idObject]
        }
        let calloutView = createCalloutView(dataObjectForMap)
        view.detailCalloutAccessoryView = calloutView
        return view
    }
}

// MARK: CLLocationManagerDelegate

extension MapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0] as CLLocation
        focusUser(location: userLocation)
    }
}

// MARK: ContainerViewControllersDelegate

extension MapViewController: ContainerViewControllersDelegate {
    func updateData(_ containerViewControllers: ContainerViewControllers) {
        updateDate()
        setObjectsInMap()
    }

    func changeVisibleObjects(_ containerViewControllers: ContainerViewControllers) {
        hideObjects = containerViewControllers.hideObjects
        updateObjectsInMap()
    }
}
