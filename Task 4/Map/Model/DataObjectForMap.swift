//
//  DataObjectForMap.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 6.03.22.
//

import Foundation
import MapKit

struct DataObjectForMap {
    let object: Any
    let type: TypeObject
    let idObject: Int
    let locationCoordinate2D: CLLocationCoordinate2D
    let addressLine: String
    let workTime: String
    let currency: String
    let cashIn: String
}
