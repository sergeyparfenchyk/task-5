//
//  ConstantString.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 23.02.22.
//

import Foundation

struct ConstantString {
    static let atmId = "Уникальный идентификатор"
    static let type = "Тип"
    static let baseCurrency = "Основная валюта"
    static let currency = "Все доступные валюты"
    static let cards = "Платёжные карты, доступные для использования"
    static let currentStatus = "Статус работоспособности"
    static let currentStatusOn = "Доступно"
    static let currentStatusOff = "Не доступно"
    static let currentStatusTempOff = "Временно недоступен"
    static let address = "Адрес"
    static let streetName = "Название улицы"
    static let buildingNumber = "Номер здания"
    static let townName = "Населенный пункт"
    static let countrySubDivision = "Регион страны"
    static let country = "Название страны в кодированной форме"
    static let addressLine = "Место установки"
    static let addresDescription = "Дополнительная информация "
    static let сoordinates = "Координаты"
    static let description = "Дополнительная информация "
    static let availability = "Режим работы"
    static let access24Hours = "Круглосуточно"
    static let isRestricted = "Доступ к объекту ограничен системой пропуска и прочее"
    static let sameAsOrganization = "Режим работы зависит от режима работы другого объекта"
    static let phoneNumber = "Номер телефона"

    static let serviceTypeCashWithdrawal = "Снятие наличных"
    static let serviceTypePinChange = "Смена ПИН"
    static let serviceTypePINUnblock = "Разблокировка ПИН"
    static let serviceTypePINActivation = "Активация ПИН"
    static let serviceTypeBalance = "Просмотр баланса"
    static let serviceTypeMiniStatement = "Выписка"
    static let serviceTypeBillPayments = "Платежи"
    static let serviceTypeMobileBankingRegistration = "Регистрация мобильного банка"
    static let serviceTypeCurrencyExhange = "Обмен валют"
    static let serviceTypeCashIn = "Пополнение наличными"
    static let serviceTypeOther = "Другие"

    static let yes = "Да"
    static let not = "Нет"

    static let imageCheckMarkSquare = "checkmark.square"
    static let imageSquare = "square"

    static let good = "Хорошо"
    static let errorDownload = "Ошибка загрузки"
}
