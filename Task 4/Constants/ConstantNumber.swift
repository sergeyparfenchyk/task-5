//
//  ConstantNumber.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 5.03.22.
//

import Foundation

struct ConstantNumber {
    static let defaultLatitude = 52.425163
    static let defaultLongitude = 31.015039
}
