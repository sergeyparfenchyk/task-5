//
//  ConstantNotificationName.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 26.02.22.
//

import Foundation

struct ConstantNotificationName {
    static let didSetAtms = Notification.Name("didSetAtms")
}
