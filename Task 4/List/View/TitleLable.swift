//
//  TitleLable.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 26.02.22.
//

import UIKit

class TitleLable: UILabel {

    // MARK: Initialize

    init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        font = UIFont.boldSystemFont(ofSize: 12)
        adjustsFontSizeToFitWidth = true
    }
}
