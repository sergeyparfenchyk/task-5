//
//  NameCityCollectionReusableView.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 27.02.22.
//

import UIKit

// MARK: Constants

private struct ConstantSize {
    static let fontNameCity = 20.0
    static let leadingOffset = 5.0
}

class NameCityCollectionReusableView: UICollectionReusableView {

    // MARK: Private Properties

    private(set) lazy var nameCityLable: UILabel = {
        let nameCityLable = UILabel()
        nameCityLable.font = UIFont.boldSystemFont(ofSize: ConstantSize.fontNameCity)
        addSubview(nameCityLable)
        nameCityLable.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(ConstantSize.leadingOffset)
            make.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
        }
        return nameCityLable
    }()
}
