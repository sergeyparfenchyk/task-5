//
//  AtmCollectionViewCell.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 26.02.22.
//

import UIKit

// MARK: Constants

private extension ConstantColor {
    static let backgroundCell = UIColor.systemGray6
}

private struct ConstantSize {
    static let cornerRadiusCell = 5.0
    static let heighTitle = 20.0
    static let heighPlaceDescription = 40.0
    static let heighCurrencyDescription = 16.0
    static let numberOfLinesDescription = 4
}

private extension ConstantString {
    static let placeTitleLable = "Место установки"
    static let workingHoursTitleLable = "Режим работы"
    static let currencyTitleLable = "Выдаваемая валюта"
}

class AtmCollectionViewCell: UICollectionViewCell {

    // MARK: Private properties

    private(set) lazy var placeTitleLable = TitleLable()
    private(set) lazy var workingHoursTitleLable = TitleLable()
    private(set) lazy var currencyTitleLable = TitleLable()

    private(set) lazy var placeDescriptionLable: DescriptionLable = {
        let placeDescriptionLable = DescriptionLable()
        placeDescriptionLable.numberOfLines = ConstantSize.numberOfLinesDescription
        return placeDescriptionLable
    }()

    private(set) lazy var workingHoursDescriptionLable: DescriptionLable = {
        let workingHoursDescriptionLable = DescriptionLable()
        workingHoursDescriptionLable.numberOfLines = ConstantSize.numberOfLinesDescription
        return workingHoursDescriptionLable
    }()

    private(set) lazy var currencyDescriptionLable = DescriptionLable()

    // MARK: Initialization

    override init(frame: CGRect) {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        addSubviews()
        setConstraint()
        backgroundColor = ConstantColor.backgroundCell
        layer.cornerRadius = ConstantSize.cornerRadiusCell
        layer.masksToBounds = true
        placeTitleLable.text = ConstantString.placeTitleLable
        workingHoursTitleLable.text = ConstantString.workingHoursTitleLable
        currencyTitleLable.text = ConstantString.currencyTitleLable
    }

    private func addSubviews() {
        contentView.addSubview(placeTitleLable)
        contentView.addSubview(workingHoursTitleLable)
        contentView.addSubview(currencyTitleLable)
        contentView.addSubview(placeDescriptionLable)
        contentView.addSubview(workingHoursDescriptionLable)
        contentView.addSubview(currencyDescriptionLable)
    }

    private func setConstraint() {
        placeTitleLable.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(ConstantSize.heighTitle)
        }
        workingHoursTitleLable.snp.makeConstraints { make in
            make.top.equalTo(placeDescriptionLable.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(ConstantSize.heighTitle)
        }
        currencyTitleLable.snp.makeConstraints { make in
            make.bottom.equalTo(currencyDescriptionLable.snp.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(ConstantSize.heighTitle)
        }
        placeDescriptionLable.snp.makeConstraints { make in
            make.top.equalTo(placeTitleLable.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(ConstantSize.heighPlaceDescription)
        }
        workingHoursDescriptionLable.snp.makeConstraints { make in
            make.bottom.equalTo(currencyTitleLable.snp.top)
            make.top.equalTo(workingHoursTitleLable.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
        currencyDescriptionLable.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(ConstantSize.heighCurrencyDescription)
        }
    }
}
