//
//  AllObject.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 5.03.22.
//

import Foundation
import CoreLocation

struct AllObject {
    let object: Any
    let typeObject: TypeObject
    let locationCoordinate2D: CLLocationCoordinate2D
    let townName: String
    let addressLine: String
    let workTime: String
    let currency: String
}
