//
//  ListViewController.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 23.02.22.
//

import UIKit
import MapKit

// MARK: Constants

private extension ConstantString {
    static let cellId = "CellId"
    static let sectionId = "SectionId"
}

private struct ConstantSize {
    static let headerSection = 20.0
    static let minimumLineSpacingForSectionAt = 10.0
    static let minimumInteritemSpacingForSectionAt = 10.0
    static let numberRows = 3
    static let heightCell = 150.0
    static let offsetCell = 10.0
}

class ListViewController: UIViewController, CLLocationManagerDelegate {

    // MARK: Public Properties
    var atmResponse = ATMResponse()
    var infoBoxResponse = InfoBoxResponse()
    var branchResponse = BranchResponse()
    var hideObjects: Set<TypeObject> = []

    // MARK: Private Properties

    private let locationManager = CLLocationManager()
    private var allObjects: [AllObject] = []
    private var cellsInSection = [0]
    private var titlesSection: [String] = []
    private lazy var atmCollectionView: UICollectionView = {
        atmCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        atmCollectionView.register(AtmCollectionViewCell.self, forCellWithReuseIdentifier: ConstantString.cellId)
        atmCollectionView.register(NameCityCollectionReusableView.self,
                                   forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                   withReuseIdentifier: ConstantString.sectionId)
        atmCollectionView.delegate = self
        atmCollectionView.dataSource = self
        return atmCollectionView
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        setContraints()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let containerViewControllers = parent as? ContainerViewControllers {
            containerViewControllers.delegate = self
        }
        updateAllData()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        atmCollectionView.reloadData()
    }

    // MARK: Private Method

    private func addSubviews() {
        view.addSubview(atmCollectionView)
    }

    private func updateAllData() {
        updateATMData()
        updateInfoBoxData()
        updateBranchData()
        sortedSmallInfoObject()
        countRowAndSection()
        atmCollectionView.reloadData()
    }

    private func updateATMData() {
        var tmpAllDataObjects: [AllObject] = []
        if !hideObjects.contains(.atm) {
            for atm in atmResponse.data.atm {
                if let latitude = Double(atm.address.geolocation.geographicCoordinates.latitude),
                   let longitude = Double(atm.address.geolocation.geographicCoordinates.longitude) {
                    let locationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    let smallInfoObject = AllObject(object: atm,
                                                    typeObject: .atm,
                                                    locationCoordinate2D: locationCoordinate2D,
                                                    townName: atm.address.townName,
                                                    addressLine: atm.address.addressLine,
                                                    workTime: atm.createWorkingHoursList(),
                                                    currency: atm.currency)
                    tmpAllDataObjects.append(smallInfoObject)
                }
            }
        }
        allObjects.removeAll { smallInfoObject in
            if smallInfoObject.object as? ATMResponse.DataClass.ATM == nil {
                return false
            }
            return true
        }
        allObjects += tmpAllDataObjects
    }
    private func updateBranchData() {
        var tmpAllDataObjects: [AllObject] = []
        if !hideObjects.contains(.branch) {
            for branch in branchResponse.data.branch {
                if let latitude = Double(branch.address.geoLocation.geographicCoordinates.latitude),
                   let longitude = Double(branch.address.geoLocation.geographicCoordinates.longitude) {
                    let locationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    let smallInfoObject = AllObject(object: branch,
                                                    typeObject: .branch,
                                                    locationCoordinate2D: locationCoordinate2D,
                                                    townName: branch.address.townName,
                                                    addressLine: branch.address.addressLine,
                                                    workTime: branch.createWorkingHoursList(),
                                                    currency: "")
                    tmpAllDataObjects.append(smallInfoObject)
                }
            } }
        allObjects.removeAll { smallInfoObject in
            if smallInfoObject.object as? BranchResponse.DataClass.Branch == nil {
                return false
            }
            return true
        }
        allObjects += tmpAllDataObjects
    }

    private func updateInfoBoxData() {
        var tmpAllDataObjects: [AllObject] = []
        if !hideObjects.contains(.infobox) {
            for infoBox in infoBoxResponse.data.infobox {
                if let latitude = Double(infoBox.latitude),
                   let longitude = Double(infoBox.longitude) {
                    let locationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    let smallInfoObject = AllObject(object: infoBox,
                                                    typeObject: .infobox,
                                                    locationCoordinate2D: locationCoordinate2D,
                                                    townName: infoBox.townName,
                                                    addressLine: infoBox.addressLine,
                                                    workTime: infoBox.workTime,
                                                    currency: infoBox.currency)
                    tmpAllDataObjects.append(smallInfoObject)
                }
            } }
        allObjects.removeAll { smallInfoObject in
            if smallInfoObject.object as? InfoBoxResponse.DataClass.Infobox == nil {
                return false
            }
            return true
        }
        allObjects += tmpAllDataObjects
    }

    private func sortedSmallInfoObject() {
        allObjects = allObjects.sorted(by: { objectFirts, objectSecond in

            if objectFirts.townName < objectSecond.townName {
                return true
            } else if objectFirts.townName > objectSecond.townName {
                return false
            } else {
                var latitude = ConstantNumber.defaultLatitude
                var longitude = ConstantNumber.defaultLongitude
                if let locValue: CLLocationCoordinate2D = locationManager.location?.coordinate {
                    latitude = locValue.latitude
                    longitude = locValue.longitude
                }
                let firstDistances = sqrt(pow(latitude - objectFirts.locationCoordinate2D.latitude, 2) +
                                          (pow(longitude - objectFirts.locationCoordinate2D.longitude, 2)))
                let secondDistances = sqrt(pow(latitude - objectSecond.locationCoordinate2D.latitude, 2) +
                                           (pow(longitude - objectSecond.locationCoordinate2D.longitude, 2)))
                if firstDistances < secondDistances {
                    return true
                }
            }
            return false
        })
    }

    @objc private func setAtms(notification: Notification) {
        if allObjects.count > 0 { reloadDataAtmCollectionView() }
    }

    private func reloadDataAtmCollectionView() {
        cellsInSection = [0]
        countRowAndSection()
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {
                return
            }
            self.atmCollectionView.reloadData()
        }
    }

    private func setContraints() {
        atmCollectionView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.trailing.equalToSuperview()
        }
    }

    private func countRowAndSection() {
        cellsInSection = [0]
        titlesSection = []
        var section = 0
        var index = 1
        while index < allObjects.count {
            if allObjects[index - 1].townName != allObjects[index].townName {
                cellsInSection[section] += 1
                cellsInSection.append(0)
                titlesSection.append(allObjects[index - 1].townName)
                section += 1
            } else {
                cellsInSection[section] += 1
            }
            index += 1
        }
        if cellsInSection != [0] {
            cellsInSection[section] += 1
        } else {
            cellsInSection = []
        }
    }
}

// MARK: - UICollectionViewDataSource

extension ListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return cellsInSection[section]
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return cellsInSection.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if let headerSection = collectionView.dequeueReusableSupplementaryView(
            ofKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: ConstantString.sectionId,
            for: indexPath) as? NameCityCollectionReusableView {

            if  titlesSection.count > indexPath.section {
                headerSection.nameCityLable.text = titlesSection[indexPath.section]
            }
            return headerSection
        }
        let headerSection = NameCityCollectionReusableView()
        return headerSection
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {

        return CGSize(width: collectionView.bounds.size.width, height: ConstantSize.headerSection)
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConstantString.cellId,
                                                         for: indexPath) as? AtmCollectionViewCell {
            let item = indexPath.item + indexPath.section
            cell.placeDescriptionLable.text = allObjects[item].addressLine
            cell.workingHoursDescriptionLable.text = allObjects[item].workTime
            cell.currencyDescriptionLable.text = allObjects[item].currency
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ConstantString.cellId, for: indexPath)
        return cell
    }
}

// MARK: UICollectionViewDelegate

extension ListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = indexPath.item + indexPath.section
        let allInformationObjectViewController = AllInformationObjectViewController()
        allInformationObjectViewController.modalPresentationStyle = .pageSheet
        switch allObjects[item].typeObject {
        case .atm:
            allInformationObjectViewController.typeObject = .atm
            allInformationObjectViewController.object = allObjects[item].object
            allInformationObjectViewController.locationCoordinate2D = allObjects[item].locationCoordinate2D
            present(allInformationObjectViewController, animated: true)
        case .infobox:
            allInformationObjectViewController.typeObject = .infobox
            allInformationObjectViewController.object = allObjects[item].object
            allInformationObjectViewController.locationCoordinate2D = allObjects[item].locationCoordinate2D
            present(allInformationObjectViewController, animated: true)
        case .branch:
            allInformationObjectViewController.typeObject = .branch
            allInformationObjectViewController.object = allObjects[item].object
            allInformationObjectViewController.locationCoordinate2D = allObjects[item].locationCoordinate2D
            present(allInformationObjectViewController, animated: true)
        }
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension ListViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (view.frame.width - ConstantSize.offsetCell *
                              Double((ConstantSize.numberRows - 1))) /
                      Double(ConstantSize.numberRows),
                      height: ConstantSize.heightCell)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ConstantSize.minimumLineSpacingForSectionAt
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return ConstantSize.minimumInteritemSpacingForSectionAt
    }
}

// MARK: ContainerViewControllersDelegate

extension ListViewController: ContainerViewControllersDelegate {
    func updateData(_ containerViewControllers: ContainerViewControllers) {
        hideObjects = containerViewControllers.hideObjects
        updateAllData()
    }

    func changeVisibleObjects(_ containerViewControllers: ContainerViewControllers) {
        hideObjects = containerViewControllers.hideObjects
        updateAllData()
    }
}
