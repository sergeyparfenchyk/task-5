//
//  Branch+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 5.03.22.
//

import Foundation

extension BranchResponse.DataClass.Branch {

    // MARK: Private Method
    private func getCBU() -> String {
        if let cbu = cbu {
            return cbu
        }
        return ""
    }

    private func getAccountNumber() -> String {
        if let accountNumber = accountNumber {
            return accountNumber
        }
        return ""
    }

    // MARK: Publuc Method
    func createWorkingHoursList() -> String {
        let days = self.information.availability.standardAvailability.day
            let daysString = ["пн", "вт", "ср", "чт", "пт", "сб", "вс"]
            var workingHoursList: [String] = []
            for day in days {
                var tmpStr = ""
                tmpStr += "\(day.openingTime.dropLast(9))-\(day.closingTime.dropLast(9))"
                if day.dayBreak.breakFromTime.dropLast(9) != day.dayBreak.breakToTime.dropLast(9) {
                    tmpStr += " Перерыв \(day.dayBreak.breakFromTime.dropLast(9))-" +
                    "\(day.dayBreak.breakToTime.dropLast(9))"
                }
                workingHoursList.append(tmpStr)
            }
            var index = 1
            var tmpDay = "\(daysString[0])"
            var tmpWorkingHours = "\(daysString[0])"
            while index < workingHoursList.count {
                if workingHoursList[index - 1] != workingHoursList[index] {
                    if tmpDay != daysString[index - 1] {
                        tmpWorkingHours +=
                        "-\(daysString[index - 1]) \(workingHoursList[index - 1]) \(daysString[index])"
                        tmpDay = daysString[index]
                    } else {
                        tmpWorkingHours += " \(workingHoursList[index - 1]) \(daysString[index])"
                    }
                }
                index += 1
            }
            if tmpDay != daysString[index - 1] {
                tmpWorkingHours += "-\(daysString[index - 1]) \(workingHoursList[index - 1])"
            } else {
                tmpWorkingHours += " \(workingHoursList[index - 1])"
            }
            return tmpWorkingHours
    }
}

// MARK: All details
extension BranchResponse.DataClass.Branch {
    static let detailTitle = ["Уникальный идентификатор",
                              "Наименование отделения",
                              "Номер ЦБУ. Отображается только для ЦБУ",
                              "Номер рассчетного счета ЦБУ",
                              "Наличие Wi-Fi",
                              "Наличие электронной очереди",
                              "Название улицы или проспекта",
                              "Номер здания",
                              "Номер корпуса здания",
                              "Почтовый индекс",
                              "Название населенного пункта",
                              "Название региона страны",
                              "Широта",
                              "Долгота",
                              "Режим работы"]

    func getDetailDescription() -> [String] {
        let detailDescription = ["\(branchID)",
                                 "\(name)",
                                 "\(getCBU())",
                                 "\(getAccountNumber())",
                                 "\(wifi == 0 ? ConstantString.not : ConstantString.yes)",
                                 "\(equeue == 0 ? ConstantString.not : ConstantString.yes )",
                                 "\(address.streetName)",
                                 "\(address.buildingNumber)",
                                 "\(address.department)",
                                 "\(address.postCode)",
                                 "\(address.townName)",
                                 "\(address.countrySubDivision)",
                                 "\(address.geoLocation.geographicCoordinates.latitude)",
                                 "\(address.geoLocation.geographicCoordinates.longitude)",
                                 "\(createWorkingHoursList())"]
        return detailDescription
    }
}
