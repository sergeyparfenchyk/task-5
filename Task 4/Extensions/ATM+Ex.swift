//
//  ATM+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 27.02.22.
//

import Foundation

extension ATMResponse.DataClass.ATM {
    func createWorkingHoursList() -> String {
        let days = self.availability.standardAvailability.day
        let daysString = ["пн", "вт", "ср", "чт", "пт", "сб", "вс"]
        var workingHoursList: [String] = []
        for day in days {
            var tmpStr = ""
            tmpStr += "\(day.openingTime)-\(day.closingTime)"
            if day.dayBreak.breakFromTime != day.dayBreak.breakToTime {
                tmpStr += " Перерыв \(day.dayBreak.breakFromTime)-\(day.dayBreak.breakToTime)"
            }
            workingHoursList.append(tmpStr)
        }
        var index = 1
        var tmpDay = "\(daysString[0])"
        var tmpWorkingHours = "\(daysString[0])"
        while index < workingHoursList.count {
            if workingHoursList[index - 1] != workingHoursList[index] {
                if tmpDay != daysString[index - 1] {
                    tmpWorkingHours +=
                    "-\(daysString[index - 1]) \(workingHoursList[index - 1]) \(daysString[index])"
                    tmpDay = daysString[index]
                } else {
                    tmpWorkingHours += " \(workingHoursList[index - 1]) \(daysString[index])"
                }
            }
            index += 1
        }
        if tmpDay != daysString[index - 1] {
            tmpWorkingHours += "-\(daysString[index - 1]) \(workingHoursList[index - 1])"
        } else {
            tmpWorkingHours += " \(workingHoursList[index - 1])"
        }
        return tmpWorkingHours
    }

    func isCachIn() -> Bool {
        let services = self.services
        for service in services where service.serviceType == "CashIn" {
            return true
        }
        return false
    }

    func getCards() -> String {
        var allCards = ""
        for card in cards {
            allCards += "\(card) "
        }
        return allCards
    }

    func getCurrentStatus() -> String {
        switch currentStatus {
        case ConstantStringCheck.currentStatusOn:
            return ConstantString.currentStatusOn
        case ConstantStringCheck.currentStatusOff:
            return ConstantString.currentStatusOff
        case ConstantStringCheck.currentStatusTempOff:
            return ConstantString.currentStatusTempOff
        default:
            break
        }
        return ""
    }

    func getCordinate() -> String {
        return "\(address.geolocation.geographicCoordinates.latitude)," +
        "\(address.geolocation.geographicCoordinates.longitude)"
    }

    func getAddress() -> String {
        var fullAdress = ""
        if address.streetName != "" {
            fullAdress += "\(address.streetName), "
        }
        if address.buildingNumber != "" {
            fullAdress += "\(address.buildingNumber), "
        }
        if address.townName != "" {
            fullAdress += "\(address.townName), "
        }
        if address.countrySubDivision != "" {
            if address.countrySubDivision != "Минск" {
                fullAdress += "\(address.countrySubDivision) обл. "
            }
        }
        return fullAdress
    }

    func checkService(_ serviceType: String) -> String {
        for service in services where service.serviceType == serviceType {
            if service.serviceDescription == "" {
                return ConstantString.yes
            } else {
                return service.serviceDescription
            }
        }
        return ConstantString.not
    }

    static let detailTitle = [ConstantString.atmId,
                              ConstantString.type,
                              ConstantString.baseCurrency,
                              ConstantString.currency,
                              ConstantString.cards,
                              ConstantString.currentStatus,
                              ConstantString.address,
                              ConstantString.addressLine,
                              ConstantString.addresDescription,
                              ConstantString.сoordinates,
                              ConstantString.isRestricted,
                              ConstantString.availability,
                              ConstantString.sameAsOrganization,
                              ConstantString.phoneNumber,
                              ConstantString.serviceTypeCashWithdrawal,
                              ConstantString.serviceTypePinChange,
                              ConstantString.serviceTypePINUnblock,
                              ConstantString.serviceTypePINActivation,
                              ConstantString.serviceTypeBalance,
                              ConstantString.serviceTypeMiniStatement,
                              ConstantString.serviceTypeBillPayments,
                              ConstantString.serviceTypeMobileBankingRegistration,
                              ConstantString.serviceTypeCurrencyExhange,
                              ConstantString.serviceTypeCashIn,
                              ConstantString.serviceTypeOther]

    func getDetailDescription() -> [String] {
        let detailDescription = ["\(atmID)",
                                 "\(type)",
                                 "\(baseCurrency)",
                                 "\(currency)",
                                 "\(getCards())",
                                 "\(getCurrentStatus())",
                                 "\(getAddress())",
                                 "\(address.addressLine)",
                                 "\(address.addressDescription)",
                                 "\(getCordinate())",
                                 "\(availability.isRestricted ? ConstantString.yes : ConstantString.not)",
                                 "\(createWorkingHoursList())",
                                 "\(availability.sameAsOrganization ? ConstantString.yes : ConstantString.not)",
                                 "\(contactDetails.phoneNumber)",
                                 "\(checkService(ConstantStringCheck.serviceTypeCashWithdrawal))",
                                 "\(checkService(ConstantStringCheck.serviceTypePinChange))",
                                 "\(checkService(ConstantStringCheck.serviceTypePINUnblock))",
                                 "\(checkService(ConstantStringCheck.serviceTypePINActivation))",
                                 "\(checkService(ConstantStringCheck.serviceTypeBalance))",
                                 "\(checkService(ConstantStringCheck.serviceTypeMiniStatement))",
                                 "\(checkService(ConstantStringCheck.serviceTypeBillPayments))",
                                 "\(checkService(ConstantStringCheck.serviceTypeMobileBankingRegistration))",
                                 "\(checkService(ConstantStringCheck.serviceTypeCurrencyExhange))",
                                 "\(checkService(ConstantStringCheck.serviceTypeCashIn))",
                                 "\(checkService(ConstantStringCheck.serviceTypeOther))"]
        return detailDescription
    }
}
