//
//  Infobox+Ex.swift
//  Task 4
//
//  Created by Sergey Parfentchik on 7.03.22.
//

import Foundation

extension InfoBoxResponse.DataClass.Infobox {
    static let detailTitle = ["Уникальный идентификатор",
                              "Область",
                              "Населённый пункта",
                              "Улица",
                              "Дом",
                              "Место установки",
                              "Режим работы инфокиоска",
                              "Координата широты",
                              "Координата долготы",
                              "Перечень валют с которыми работает инфокиоск",
                              "Тип инфокиоска",
                              "Наличие купюроприемника",
                              "Исправность купюроприемника",
                              "Приёмник пачек банкнот",
                              "Возможность печати чека",
                              "Наличие платежа \"Региональные платежи\"",
                              "Наличие платежа \"Пополнение картсчета наличными\"",
                              "Исправность инфокиоска"]

    func getDetailDescription() -> [String] {
        let detailDescription = ["\(infoID)",
                                 "\(countrySubDivision)",
                                 "\(townType) \(townName)",
                                 "\(streetType) \(streetName)",
                                 "\(buildingNumber)",
                                 "\(addressLine)",
                                 "\(workTime)",
                                 "\(latitude)",
                                 "\(longitude)",
                                 "\(currency)",
                                 "\(type)",
                                 "\(cashIn)",
                                 "\(cashInCheck)",
                                 "\(typeCashIn)",
                                 "\(infPrinter)",
                                 "\(regionPlatej)",
                                 "\(popolneniePlatej)",
                                 "\(currentStatus)"]
        return detailDescription
    }
}
